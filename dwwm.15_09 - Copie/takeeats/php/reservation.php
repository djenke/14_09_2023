<?php

require_once('utils/db_connect.php');
require('utils/function.php');
require('utils/mailer.php');


isConnected();


if($_SERVER["REQUEST_METHOD"]=="POST") $method =$_POST ;
else $method = $_GET;



switch ($method['choice']) {


     case 'select':

        $req=$db->query("SELECT * FROM reservation");
        $reservations=$req->fetchALL(PDO::FETCH_ASSOC);
        echo json_encode(["success" => true, "reservations" => $reservations]);
        break;


     case 'select_id':        
//Je selectionne la date de la reservation et le nom de la table reservation ensuite le nom du restaurant j'effectue une jointure entre reservation.id_restaurant et restaurant.id_restaurant en leur donnat avant ca des alias
        $req=$db->prepare  ('SELECT r.date_reservation, r.name_reservation, res.name AS restaurant_name
        FROM reservation AS r
        INNER JOIN restaurant AS res ON r.id_restaurant = res.id_restaurant
        WHERE id_users= ?');
        $req->execute([$_SESSION['user_id']]);

        $reservation=$req->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode(["success" => true, "reservation" => $reservation]);
        break;

     case 'insert':
            if ($_SERVER["REQUEST_METHOD"] != "POST") {
                echo json_encode(["success" => false, "error" => "La méthode utilisée n'est pas la bonne"]);
                die;
            }
        
            if (!isset($method["id_restaurant"],$method["number_of_People"], $method["name_reservation"],$method["date_reservation"])) {
                echo json_encode(["success" => false, "error" => "Données manquantes"]);
                die;
            }
            if (
    
          empty(trim($method["number_of_People"])) 
            ||empty(trim($method["name_reservation"]))
            ||empty(trim($method["date_reservation"]))
            ) {
                echo json_encode(["success" => false, "error" => "Champs vides"]);
                die;
            }
            // Vérifier si l'utilisateur a déjà une réservation

            //cette requête SQL récupère toutes les colonnes de la table "reservation" pour les lignes où les valeurs de "id_users" et "id_restaurant"
            $existingReservation = $db->prepare("SELECT * FROM reservation WHERE id_users = :id_users AND id_restaurant = :id_restaurant");
            $existingReservation->bindValue(":id_users", $_SESSION['user_id']);
            $existingReservation->bindValue(":id_restaurant", $method['id_restaurant']);
            $existingReservation->execute();

            if ($existingReservation->rowCount() > 0) {
                echo json_encode(["success" => false, "error" => "Vous avez déjà une réservation en cours."]);
                die;
            }

            $req = $db->prepare("INSERT INTO reservation (id_users, id_restaurant,date_reservation,number_of_People, name_reservation) VALUES (:id_users, :id_restaurant,:date_reservation,:number_of_People, :name_reservation)");
            $req->bindValue(":id_users", $_SESSION['user_id']);
            $req->bindValue(":id_restaurant", $method['id_restaurant']);
            $req->bindValue(":date_reservation",$method ['date_reservation']);
            $req->bindValue(":number_of_People", $method['number_of_People']);
            $req->bindValue(":name_reservation", $method['name_reservation']);
            $req->execute();
            echo json_encode(["success"=> true]);
            break ;

        }