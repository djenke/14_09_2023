<?php
// Je démarre ma session
session_start();
//Dans cette page je vais écrire plusieurs fonctions qui vont me permettre de gagner du temps et d'avoir un code moins long et plus comprehensible 

/**
 * @desc Renvoie une erreur si l'utilisateur n'est pas connecté
 * @return void - Ne retourne rien
 */

 /*Je vais écrire une fonction qui se nomme isConnected qui va me permettre de determiner la connexion de l'utilisateur s'il est connecter ou non */ 
function isConnected()
{
    // Si la clé "connected" n'existe pas dans la superglobale SESSION OU que la valeur de "connected" dans la superglobale SESSION n'est pas vrai alors
    if (!isset($_SESSION["connected"]) || !$_SESSION["connected"]) {
        echo json_encode(["success" => false, "error" => "Vous n'êtes pas connecté"]);
        die;
    }
}

/**
 * @desc Renvoie une erreur si l'utilisateur n'est pas admin
 * @return void - Ne retourne rien
 */
function isAdmin()
{
    // Si la clé "admin" n'existe pas dans la superglobale SESSION OU que la valeur de "admin" dans la superglobale SESSION n'est pas vrai (!=1) alors
    if (!isset($_SESSION["admin"]) || !$_SESSION["admin"]) {
        // J'envoie une réponse avec un success false et un message d'erreur
        echo json_encode(["success" => false, "error" => "Vous n'êtes pas autorisé"]);
        die; 
    }
}
/**
 * @desc Upload une image
 * @param array $file - Contient le fichier à upload
 * @return void - Retourne le nom du fichier upload sinon retourne false
 */

