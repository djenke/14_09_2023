$(document).ready(function() {
    // Récupérez l'objet utilisateur depuis le localStorage
    const user = JSON.parse(localStorage.getItem('id_users'));

    // Vérifiez si l'utilisateur n'est pas administrateur (admin === 0)
    if (user && user.admin === 0) {
        // Supprimez le bouton "Admin" en utilisant son texte ("Admin" dans ce cas)
        $('a:contains("Admin")').remove();
    }
});